#GitBerry

The GitHub client for the BlackBerry PlayBook.


## Tools

This app will be built using HTML5, so here are the frameworks:

 * jQuery
 * ...

## TODO

 * Everything

## FAQ

**Why develop for the PlayBook?**

I know that RIM isn't going pretty well, but I really love their platform and the PlayBook is by far the best tablet I ever used.

**Do you own a PlayBook?**

At the time I don't, so I'll start this by debugging and testing everything in Chrome, and in the simulator. After I get my PlayBook (June 14th) I'll start using it to test and debug the project. I've got a PlayBook in January, but unfortunately my unit had a factory problem (which manifested four days after I got it) and I exchanged it for a ASUS Eee Pad Slider at the store because they didn't had another one in stock.

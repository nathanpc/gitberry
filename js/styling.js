// styling.js
// STYLE *ALL* THE DIVS!!!!

function setup_home_nav() {
  var topbar = 45;
  var navbar = 58;
  var padding_offset = 20;

  $("#homeNav").css("height", ($(document).height() - topbar) + "px");
  
  $("#HomeScreen").css("height", ($(document).height() - (topbar + padding_offset)) + "px");
  $("#HomeScreen").css("width", ($(document).width() - (navbar + padding_offset)) + "px");

  $("#UserScreen").css("height", ($(document).height() - (topbar + padding_offset)) + "px");
  $("#UserScreen").css("width", ($(document).width() - (navbar + padding_offset)) + "px");

  $("#RepoScreen").css("height", ($(document).height() - (topbar + padding_offset)) + "px");
  $("#RepoScreen").css("width", ($(document).width() - (navbar + padding_offset)) + "px");

  $("#SearchScreen").css("height", ($(document).height() - (topbar + padding_offset)) + "px");
  $("#SearchScreen").css("width", ($(document).width() - (navbar + padding_offset)) + "px");
}

function openScreen(navID, screen) {
  $("#homeNav ul li .selected").removeClass("selected");
  $("#homeNav ul li #" + navID).addClass("selected");
  switchToSectionWithId(screen);
}

function switchToSectionWithId(sectionId) {
  hideAllSections();
  showSectionWithId(sectionId);
}

function hideAllSections() {
  var sections = document.getElementsByTagName('section');
  for (var i = 0; i < sections.length; i++) {
    var section = sections[i];
    section.setAttribute('class', 'hidden');
  }
}

function showSectionWithId(sectionId) {
  fadeInFromOpaque("#" + sectionId);
}

function fadeOut(element) {
  $(element).animate({ "opacity": "0" }, "slow", function() {
    $(element).addClass("hidden");
  });
}

function fadeIn(element) {
  $(element).removeClass("hidden");
  $(element).animate({ "opacity": "1" }, "slow");
}

function fadeInFromOpaque(element) {
  $(element).css("opacity", "0");
  $(element).removeClass("hidden");
  $(element).animate({ "opacity": "1" }, "slow");
}

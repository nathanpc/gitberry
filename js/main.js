function loaded() {
  $(document).bind("mobileinit", function() {
    $.support.cors = true;
    $.mobile.allowCrossDomainPages = true;
  });

  setup_home_nav();

  if (localStorage.getItem("GitBookPass") == null) {
    $("#login").modal("show");
  } else {
    alert("You're logged!");
  }
}
